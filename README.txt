LIMESURVEY IMPORTER
===================

LimeSurvey Importer connects to a MySQL or PostgreSQL database and allows to import a LimeSurvey
survey, by creating a custom content type and using a Feeds Importer populate the nodes.

Currently question types S, U, N, K, Y, L, M and ! are supported.

Requirements
============

Feeds Multivalue CSV Parser
  http://drupal.org/node/759966#comment-4251172

Usage
=====
Using the menu link LimeSurvey Importer or accessing the URL /limesurvey_importer/, the importing
process is initiated. First, the database connection settings must be introduced. After querying
the database, a list of surveys is showed and one survey must be selected. When the survey is
selected a .csv file is available for download, and a new custom content type is created. Finally,
a Feeds Importer with the information of the selected survey and the custom content type is created,
allowing to import the survey's information into the new content types using the .csv file created.

